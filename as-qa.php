<?php 
/**
 * Plugin Name:  QuestionAnswer
 * Plugin URI:   https://actualsolutions.net/
 * Description:  Plugin for interaction with clients through questions and answers
 * Version:      1.0.0
 * Author:       Paul Strelkovsky
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */

defined( 'ABSPATH' ) or die();

// registering post type for questions
require_once( 'inc/posttypes.php' );

// registering taxonomies for questions categories
require_once( 'inc/taxonomies.php' );

// registering polylang
require_once( 'inc/strings.php' );

// registering shortcodes
require_once( 'inc/shortcodes.php' );

// registering actions and handlers
require_once( 'inc/actions.php' );