<?php 
defined( 'ABSPATH' ) or die();

// shortcode for main qa page
add_shortcode( 'qa_dashboard', 'asqa_pront_dashboard' );
function asqa_print_dashboard( $atts ){
	ob_start();
	require_once( 'parts/sc_asqa_print_dashboard.php' );
	return ob_get_clean();
}

// shortcode for qa categories list
add_shortcode( 'qa_categories', 'asqa_print_categories_list' );

/**
 * @param $atts - can contain key 'layout' ( grid | list )
 */
function asqa_print_categories_list( $atts ){
	ob_start();
	require_once( 'parts/sc_asqa_print_categories_list.php' );
	return ob_get_clean();	
}

add_shortcode( 'qa_form', 'asqa_print_question_form' );
/**
 * @param $atts - can contain key 'category' (  )
 * if category specified, then do not show category field
 * make it hidden
 */
function asqa_print_question_form( $atts ){
	ob_start();
	require_once( 'parts/qa_form.php' );
	return ob_get_clean();
}