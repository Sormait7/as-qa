<?php 
defined( 'ABSPATH' ) or die();

// select layout type based on attribute
if( isset( $atts[ 'layout' ] ) && $atts[ 'layout' ] == 'list' ){
	$layout = 'list';
}
else{
	$layout = 'grid';
}

// include appropriate layout file
if( $layout == 'list' ){
	require_once( 'sc_cat_list_layout/list.php' );
}
else{
	require_once( 'sc_cat_list_layout/grid.php' );
}
?>