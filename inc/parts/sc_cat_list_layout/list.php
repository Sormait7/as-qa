<?php 
defined( 'ABSPATH' ) or die();

$qa_cats = get_terms( [
	'taxonomy' => 'qacategory',
	'count' => true,
	'hide_empty' => false,
] );

if( !empty( $qa_cats ) && !is_wp_error( $qa_cats ) ){
	echo '<ul class="qa-list list-group list-group-flush">';
	foreach ($qa_cats as $cat) {
		$count = ( ( ( int ) $cat->count ) > 0 ) ? '<span class="badge badge-pill badge-success">'.$cat->count.'</span>' : '';
		echo '<li class="list-group-item d-flex justify-content-between align-items-center"><a href="'.get_term_link( $cat->term_id, 'qacategory' ).'">'.$cat->name.' '.$count.'</a></li>';
	}
	echo '</ul>';
}
else{
	echo '<p class="text-center">'.pll_translate_string( 'QA_NO_CATEGORIES', pll_current_language() ).'</p>';
}