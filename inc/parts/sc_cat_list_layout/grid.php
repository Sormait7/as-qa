<?php 
defined( 'ABSPATH' ) or die();

$qa_cats = get_terms( [
	'taxonomy' => 'qacategory',
	'count' => true,
	'hide_empty' => false,
] );

if( !empty( $qa_cats ) && !is_wp_error( $qa_cats ) ){
	echo '<div class="row">';
	foreach ($qa_cats as $cat) {
		$count = ( ( ( int ) $cat->count ) > 0 ) ? '<span class="badge badge-pill badge-success">'.$cat->count.'</span>' : '';
		?>
		<div class="col-sm-6 col-md-4">
		    <div class="card">
		      <div class="card-body">
		        <h5 class="card-title"><?php echo $cat->name.' '.$count; ?></h5>
		        <p class="card-text text-muted"><?php echo $cat->description; ?></p>
		        <a href="<?php echo get_term_link( $cat->term_id, 'qacategory' ); ?>" class="btn-rounded bg-accent-1 text-white"><?php echo pll_translate_string( 'QA_CATEGORIES_GRID_ITEM_BUTTON_TITLE', pll_current_language() ); ?></a>
		      </div>
		    </div>
		  </div>		
		<?php
	}
	echo '</div>';
}
else{
	echo '<p class="text-center">'.pll_translate_string( 'QA_NO_CATEGORIES', pll_current_language() ).'</p>';
}