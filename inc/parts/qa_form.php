<?php 
// preparing fields values

if( is_user_logged_in() ){
	$cuser_id = get_current_user_id();
	$fname = get_the_author_meta( 'first_name', $cuser_id );
	$lname = get_the_author_meta( 'last_name', $cuser_id );
	$email = wp_get_current_user()->user_email;
}
elseif( isset( $_POST['qa-fname'] ) || isset( $_POST['qa-lname'] ) || isset( $_POST['qa-mail'] ) ){
	$fname = sanitize_text_field( $_POST['qa-fname'] );
	$lname = sanitize_text_field( $_POST['qa-lname'] );
	$email = sanitize_email( $_POST['qa-mail'] );
}
else{
	$fname = '';
	$lname = '';
	$email = '';
}

if( isset( $_POST['qa-phone'] ) || isset( $_POST['qa-question-short'] ) || isset( $_POST['qa-question-extended'] ) ){
	$phone = sanitize_text_field( $_POST['qa-phone'] );
	$qshort = sanitize_text_field( $_POST['qa-question-short'] );
	$qext = sanitize_text_field( $_POST['qa-question-extended'] );
}
else{
	$phone = '';
	$qshort = '';
	$qext = '';
}

$fname_input = '<input type="text" name="qa-fname" class="input-text" placeholder="'.pll_translate_string( 'QA_FORM_FNAME_FIELD_PLACEHOLDER', pll_current_language() ).'" value="'.$fname.'" required>';
$lname_input = '<input type="text" name="qa-lname" class="input-text" placeholder="'.pll_translate_string( 'QA_FORM_LNAME_FIELD_PLACEHOLDER', pll_current_language() ).'" value="'.$lname.'" required>';
$phone_input = '<input type="tel" name="qa-phone" class="input-text" placeholder="'.pll_translate_string( 'QA_FORM_PHONE_FIELD_PLACEHOLDER', pll_current_language() ).'" value="'.$phone.'" required>';
$email_input = '<input type="email" name="qa-mail" class="input-text" placeholder="'.pll_translate_string( 'QA_FORM_EMAIL_FIELD_PLACEHOLDER', pll_current_language() ).'" value="'.$email.'" required>';

$qacategories = get_terms( [
	'taxonomy' => 'qacategory',
	'hide_empty' => false,
] );

$qshort_input = '<input type="text" name="qa-question-short" class="input-text" placeholder="'.pll_translate_string( 'QA_FORM_SQUESTION_FIELD_PLACEHOLDER', pll_current_language() ).'" value="'.$qshort.'" required>';
$qext_input = '<textarea name="qa-question-extended" placeholder="'.pll_translate_string( 'QA_FORM_LQUESTION_FIELD_PLACEHOLDER', pll_current_language() ).'">'.$qext.'</textarea>';

// checking out session segment with errors
if( isset( $_SESSION['qa-form'] ) && isset( $_SESSION['qa-form']['errors'] ) ){
	$fields = [
		'qa-fname',
		'qa-lname',
		'qa-phone',
		'qa-mail',
		'qa-category',
		'qa-question-short',
		'qa-question-extended',
		'qa-submited',
	];

	$errors_msg = '<ul>';
	foreach ( $fields as $key => $field ) {
		if( array_key_exists( $field, $_SESSION['qa-form']['errors'] ) ){
			$errors_msg .= '<li>'.$_SESSION['qa-form']['errors'][$field].'</li>';
		}
	}
	$errors_msg .= '</ul>';
	unset( $_SESSION['qa-form'] );
}

// checking out success message
if( isset( $_SESSION['qa-form'] ) && isset( $_SESSION['qa-form']['qa-add-question-success'] ) && !isset( $errors_msg ) ){
	$success_msg = pll_translate_string( 'QA_FORM_ADDED_SUCCESS', pll_current_language() );
	unset( $_SESSION['qa-form'] );
}

?>
<div class="row">
	<a name="qa-form"></a>
	<h1 class="page-title"><?php echo pll_translate_string( 'QA_FORM_TITLE', pll_current_language() ); ?></h1>
	<p><?php echo pll_translate_string( 'QA_FORM_HINT', pll_current_language() ); ?></p>
	<?php if( isset( $errors_msg ) ) : ?>
		<div class="alert alert-danger col" role="alert">
			<?php echo $errors_msg; ?>
		</div>
	<?php endif; ?>

	<?php if( isset( $success_msg ) ) : ?>
		<div class="alert alert-success col" role="alert">
			<?php echo $success_msg; ?>
		</div>
	<?php endif; ?>
</div>
<form class="row qa-form" method="post">
	<div class="row">
		<div class="col-md-3 col-sm-6">
			<?php echo $fname_input; ?>
		</div>

		<div class="col-md-3 col-sm-6">
			<?php echo $lname_input; ?>
		</div>

		<div class="col-md-3 col-sm-6">
			<!-- <input type="tel" name="qa-phone" class="input-text" placeholder="<?php echo pll_translate_string( 'QA_FORM_PHONE_FIELD_PLACEHOLDER', pll_current_language() ); ?>" required> -->
			<?php echo $phone_input; ?>
		</div>

		<div class="col-md-3 col-sm-6">
			<?php echo $email_input; ?>
		</div>

		<div class="col-md-9 col-sm-6">
			<!-- <input type="text" name="qa-question-short" class="input-text" placeholder="<?php echo pll_translate_string( 'QA_FORM_SQUESTION_FIELD_PLACEHOLDER', pll_current_language() ); ?>" required> -->
			<?php echo $qshort_input; ?>
		</div>
		<div class="col-md-3 col-sm-6">
			<select name="qa-category" class="" placeholder="<?php echo pll_translate_string( 'QA_FORM_CATEGORY_FIELD_PLACEHOLDER', pll_current_language() ); ?>">
				<option value="0"><?php echo pll_translate_string( 'QA_FORM_CATEGORY_FIELD_PLACEHOLDER', pll_current_language() ); ?></option>
				<?php
					foreach( $qacategories as $c ) {
						echo '<option value="'.$c->term_id.'">'.$c->name.'</option>';
					}
				?>
			</select>
		</div>
		<div class="col-md-12">
			<!-- <textarea name="qa-question-extended" placeholder="<?php echo pll_translate_string( 'QA_FORM_LQUESTION_FIELD_PLACEHOLDER', pll_current_language() ); ?>"></textarea> -->
			<?php echo $qext_input; ?>
		</div>
		<div class="col-sm-4">
			<?php wp_nonce_field( 'qa-add-question', 'qa-submited' ); ?>
			<!-- <input type="hidden" name="action" value="qa_add_question"> -->
			<button type="submit" class="btn-rounded bg-accent-1 text-white"><?php echo pll_translate_string( 'QA_FORM_SUBMIT_BUTTON_TEXT', pll_current_language() ); ?></button>
		</div>
	</div>
</form>