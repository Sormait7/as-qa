<?php 
// checking and handling if the question form was submited
// add_action( 'admin_post_nopriv_qa_add_question', 'asqa_handle_question_form' );
add_action( 'init', 'asqa_handle_question_form' );
function asqa_handle_question_form(){
	if ( !empty( $_POST ) && isset( $_POST['qa-submited'] ) ){
		if( wp_verify_nonce($_POST['qa-submited'], 'qa-add-question') ){
			// check and handle form data below
			$form_is_valid = true;

			// first name
			$fname = sanitize_text_field( $_POST['qa-fname'] );
			if( empty( $fname ) ){
				$_SESSION['qa-form']['errors']['qa-fname'] = pll_translate_string( 'QA_FORM_FNAME_ERROR_TEXT', pll_current_language() );
				$form_is_valid = false;
			}

			// last name
			$lname = sanitize_text_field( $_POST['qa-lname'] );
			if( empty( $lname ) ){
				$_SESSION['qa-form']['errors']['qa-lname'] = pll_translate_string( 'QA_FORM_LNAME_ERROR_TEXT', pll_current_language() );
				$form_is_valid = false;
			}

			// telephone
			$phone = sanitize_text_field( $_POST['qa-phone'] );
			if( empty( $phone ) ){
				$_SESSION['qa-form']['errors']['qa-phone'] = pll_translate_string( 'QA_FORM_PHONE_ERROR_TEXT', pll_current_language() );
				$form_is_valid = false;
			}

			// email
			$mail = sanitize_email( $_POST['qa-mail'] );
			if( empty( $mail ) ){
				$_SESSION['qa-form']['errors']['qa-mail'] = pll_translate_string( 'QA_FORM_EMAIL_ERROR_TEXT', pll_current_language() );
				$form_is_valid = false;
			}

			// category
			$category = intval( sanitize_text_field( $_POST['qa-category'] ) );
			if( empty( $category ) || ( $category == 0 ) ){
				$_SESSION['qa-form']['errors']['qa-category'] = pll_translate_string( 'QA_FORM_CATEGORY_ERROR_TEXT', pll_current_language() );
				$form_is_valid = false;
			}
			
			// question title
			$qshort = sanitize_text_field( $_POST['qa-question-short'] );
			if( empty( $qshort ) ){
				$_SESSION['qa-form']['errors']['qa-question-short'] = pll_translate_string( 'QA_FORM_SQUESTION_ERROR_TEXT', pll_current_language() );
				$form_is_valid = false;
			}

			$qext = sanitize_text_field( $_POST['qa-question-extended'] );

			if( $form_is_valid ){
				// save data, save notification, redirect to prevent sending same data
				$pid = wp_insert_post( [
					'post_title' => $qshort,
					'post_content' => $qext,
					'post_status' => 'draft',
					'post_author' => -1,
					'post_type' => 'qa',
				], 0 );

				if( $pid > 0 ){
					wp_set_post_terms( $pid, [$category], 'qacategory' );
					update_post_meta( $pid, 'author_first_name', $fname );
					update_post_meta( $pid, 'author_last_name', $lname );
					update_post_meta( $pid, 'name_and_lastname', $fname.' '.$lname );
				}

				$_SESSION['qa-form']['qa-add-question-success'] = pll_translate_string( 'QA_FORM_ADDED_SUCCESS', pll_current_language() );
			}
		}
		else{
			$_SESSION['qa-form']['errors']['qa-submited'] = pll_translate_string( 'QA_FORM_CSRF_ERROR_TEXT', pll_current_language() );
		}
	}
}

?>