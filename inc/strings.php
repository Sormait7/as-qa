<?php 
add_action( 'plugins_loaded', 'asqa_register_strings' );
function asqa_register_strings(){
	/**
	 * categories section
	 */
	pll_register_string('QA_CATEGORIES_GRID_ITEM_BUTTON_TITLE', 'QA_CATEGORIES_GRID_ITEM_BUTTON_TITLE', 'Question&Answer' );
	pll_register_string('QA_NO_CATEGORIES', 'QA_NO_CATEGORIES', 'Question&Answer' );

	/**
	 * category content section
	 */

	// Title of the question form
	pll_register_string( 'QA_FORM_TITLE', 'QA_FORM_TITLE', 'Question&Answer' );

	// Hint to the form, renders before form
	pll_register_string( 'QA_FORM_HINT', 'QA_FORM_HINT', 'Question&Answer', true );

	// first name field placeholder
	pll_register_string( 'QA_FORM_FNAME_FIELD_PLACEHOLDER', 'QA_FORM_FNAME_FIELD_PLACEHOLDER', 'Question&Answer' );

	// last name field placeholder
	pll_register_string( 'QA_FORM_LNAME_FIELD_PLACEHOLDER', 'QA_FORM_LNAME_FIELD_PLACEHOLDER', 'Question&Answer' );

	// category field default value title
	pll_register_string( 'QA_FORM_CATEGORY_FIELD_PLACEHOLDER', 'QA_FORM_CATEGORY_FIELD_PLACEHOLDER', 'Question&Answer' );

	// phone field placeholder
	pll_register_string( 'QA_FORM_PHONE_FIELD_PLACEHOLDER', 'QA_FORM_PHONE_FIELD_PLACEHOLDER', 'Question&Answer' );

	// email field placeholder
	pll_register_string( 'QA_FORM_EMAIL_FIELD_PLACEHOLDER', 'QA_FORM_EMAIL_FIELD_PLACEHOLDER', 'Question&Answer' );

	// short quetion field placeholder
	pll_register_string( 'QA_FORM_SQUESTION_FIELD_PLACEHOLDER', 'QA_FORM_SQUESTION_FIELD_PLACEHOLDER', 'Question&Answer' );

	// extended question ( description ) field placeholder
	pll_register_string( 'QA_FORM_LQUESTION_FIELD_PLACEHOLDER', 'QA_FORM_LQUESTION_FIELD_PLACEHOLDER', 'Question&Answer' );

	// submit button text
	pll_register_string( 'QA_FORM_SUBMIT_BUTTON_TEXT', 'QA_FORM_SUBMIT_BUTTON_TEXT', 'Question&Answer' );

	/**
	 * form errors and notifications section
	 */

	// first name field error
	pll_register_string( 'QA_FORM_FNAME_ERROR_TEXT', 'QA_FORM_FNAME_ERROR_TEXT', 'Question&Answer' );

	// last name field error
	pll_register_string( 'QA_FORM_LNAME_ERROR_TEXT', 'QA_FORM_LNAME_ERROR_TEXT', 'Question&Answer' );

	// phone error
	pll_register_string( 'QA_FORM_PHONE_ERROR_TEXT', 'QA_FORM_PHONE_ERROR_TEXT', 'Question&Answer' );

	// email field error
	pll_register_string( 'QA_FORM_EMAIL_ERROR_TEXT', 'QA_FORM_EMAIL_ERROR_TEXT', 'Question&Answer' );

	// category field error
	pll_register_string( 'QA_FORM_CATEGORY_ERROR_TEXT', 'QA_FORM_CATEGORY_ERROR_TEXT', 'Question&Answer' );

	// short question field error
	pll_register_string( 'QA_FORM_SQUESTION_ERROR_TEXT', 'QA_FORM_SQUESTION_ERROR_TEXT', 'Question&Answer' );

	// csrf field error
	pll_register_string( 'QA_FORM_CSRF_ERROR_TEXT', 'QA_FORM_CSRF_ERROR_TEXT', 'Question&Answer' );
	
	// question added successfull message
	pll_register_string( 'QA_FORM_ADDED_SUCCESS', 'QA_FORM_ADDED_SUCCESS', 'Question&Answer' );
}
?>