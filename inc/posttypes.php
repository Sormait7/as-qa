<?php 
defined( 'ABSPATH' ) or die();

function asqa_register_posttypes(){
	register_post_type('qa', array(
		'labels'             => array(
			'name'               => 'Вопросы',
			'singular_name'      => 'Вопрос',
			'add_new'            => 'Добавить новый',
			'add_new_item'       => 'Добавить новый вопрос',
			'edit_item'          => 'Редактировать вопрос',
			'new_item'           => 'Новый вопрос',
			'view_item'          => 'Посмотреть вопрос',
			'search_items'       => 'Найти вопрос',
			'not_found'          => 'Вопросов не найдено',
			'not_found_in_trash' => 'В корзине вопросы не найдены',
			'menu_name'          => 'Вопросы'
		  ),
		'public'             => true,
		'hierarchical'       => true,
		'menu_position'      => 30,
		'supports'           => array('title','editor', 'comments'),
		'exclude_from_search' => false,
		'show_ui' 			=> true,
		'show_in_menu' 		=> true,
		'menu_icon' 		=> 'dashicons-format-chat',
	) );
}
add_action( 'init', 'asqa_register_posttypes' );