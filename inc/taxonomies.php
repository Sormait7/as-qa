<?php
defined( 'ABSPATH' ) or die();

add_action( 'init', 'asqa_dieta_taxonomies' );
function asqa_dieta_taxonomies(){
	register_taxonomy('qacategory', array('qa'), array(
		'label'                 => 'Категории вопросов',
		'labels'                => array(
			'name'              => 'Категории вопросов',
			'singular_name'     => 'Категория вопросов',
			'search_items'      => 'Искать категории вопросов',
			'all_items'         => 'Все категории вопросов',
			'view_item '        => 'Смотреть категорию вопросов',
			'parent_item'       => 'Родительская категория вопросов',
			'parent_item_colon' => 'Родительская категория вопросов:',
			'edit_item'         => 'Редактировать категорию вопросов',
			'update_item'       => 'Обновить категорию вопросов',
			'add_new_item'      => 'Добавить категорию вопросов',
			'new_item_name'     => 'Новая категория вопросов',
			'menu_name'         => 'Категории вопросов',
		),
		'description'           => '', 
		'public'                => true,
		'show_in_menu'          => true, 
		'hierarchical'          => true,
		// 'rewrite'               => true,
		// 'capabilities'          => array(),
		'show_admin_column'     => true,
		'show_in_quick_edit'    => true,
	) );
}